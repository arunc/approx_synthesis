// Copyright (C) AGRA - University of Bremen
//
// LICENSE : GNU GPLv3
// 
// @author : Arun <arun@uni-bremen.de>
// @file   : miter_utils.hpp
//------------------------------------------------------------------------------
#pragma once

#ifndef MITER_UTILS_HPP
#define MITER_UTILS_HPP

#include <iostream>
#include <string>
#include <cassert>
#include <fstream>
#include <algorithm>

#include <types/Design.hpp>
#include <types/Port.hpp>

namespace minikit {

void write_worst_case_miter (
  const Design &orig, const Port &port_to_check,
  const std::string &miter_module, const std::string &ofile, const unsigned &limit);

void write_bit_flip_miter (
  const Design &orig, const Port &port_to_check,
  const std::string &miter_module, const std::string &ofile, const unsigned &limit);

void write_wc_bf_miter ( const Design &orig, const Port &port_to_check,
			 const std::string &miter_module, const std::string &ofile, 
			 const unsigned &wc_limit, const unsigned &bf_limit);
void write_ge_bit_flip_miter (
  const Design &orig, const Port &port_to_check,
  const std::string &miter_module, const std::string &ofile, const unsigned &limit);


}
#endif
//------------------------------------------------------------------------------

// Local Variables:
// c-basic-offset: 2
// eval: (c-set-offset 'substatement-open 0)
// eval: (c-set-offset 'innamespace 0)
// End:
