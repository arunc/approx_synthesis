# Approximate Synthesis 
------------------------------------------------------------------------------
## Arun Chandrasekharan < arun@uni-bremen.de >

### Group of Computer Architecture, (AGRA)
### Dept. of Computer Science - University of Bremen, Germany.
### http://www.informatik.uni-bremen.de/agra/eng/index.php
------------------------------------------------------------------------------
### License : GPL-V3
------------------------------------------------------------------------------

Synthesis framework for approximate circuits.

```
Builds approximate-circuit netlist from RTL specification for a given
error-rate or other error threshold parameters. The resulting circuit will
be faster and/or with lesser area but with errors.

This project is forked out of "Minikit"



-> Minikit
-> ------------------------------------------------------------------------------
-> Arun Chandrasekharan <arun@uni-bremen.de>
-> Group of Computer Architecture, (AGRA)
-> Dept. of Computer Science - University of Bremen, Germany.
-> http://www.informatik.uni-bremen.de/agra/eng/index.php
-> ------------------------------------------------------------------------------
-> License : GPL-V3
-> ------------------------------------------------------------------------------
-> 
-> Minikit is a collection of software libraries and framework.
-> Adapted (~ copied) from CirKit, all(most) individual works have been removed,
-> retaining only the external libraries related to formal verification.
-> Purpose is mainly personal (share and collaborate with people other than AGRA),
-> play around with these libs in an independent way, get a much more simpler and
-> complete build system and of course get a first hand experience in the build
-> system.
-> 
-> Read the minikit.pdf for notes on building and usage.
-> 
-> # Note: Continously modified and updated. Hence before starting a new
-> algorithm or application, checkout the branch initial-version and start from a
-> clean slate.
-> $ git checkout initial-version
-> This is especially important when publishing to outside world, else many extra
-> or unwanted stuffs will creep in.
-> -------------------------------------------------------------------------------
```
