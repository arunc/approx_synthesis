# Copyright (C) AGRA - University of Bremen
#
# LICENSE : GNU GPLv3

project("Minikit" CXX)
cmake_minimum_required(VERSION 3.0)

include(utils/cmake/CCache.cmake)
include(utils/cmake/CompilerFlags.cmake)
include(utils/cmake/ListDir.cmake)
include(utils/cmake/CMakeProcedures.cmake)

message ("TOP-DIR :: ${CMAKE_SOURCE_DIR}")
set(ext_BOOST ${CMAKE_SOURCE_DIR}/ext/boost)
include_directories(${ext_BOOST} ext/include)
add_subdirectory(ext)

set( CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/programs )

# Options
option(minikit_ENABLE_PROGRAMS "build programs" on)
option(minikit_BUILD_SHARED "build shared libraries" on)
set(minikit_PACKAGES "" CACHE STRING "if non-empty, then only the packages in the semicolon-separated lists are build")

# Libraries (include)
include_directories(src)
add_subdirectory(src)


